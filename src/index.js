import http from "http";
import https from "https";
import url from "url";
import { StringDecoder } from "string_decoder";
import fs from "fs";
import path from "path";
import { fileURLToPath } from "url";

import config from "./config/index.js";

function log({ method, headers, path, queryString, payload }) {
  console.log(`[${method}] /${path}\n`);
  console.log(`HEADERS\n${JSON.stringify(headers, null, 2)}\n`);
  console.log(`QUERY STRING\n${JSON.stringify(queryString, null, 2)}\n`);
  console.log(`PAYLOAD\n${payload}`);
}

function handleRequest(request, response) {
  const parsedURL = url.parse(request.url, true);

  const { headers } = request;

  const method = request.method.toUpperCase();

  const path = parsedURL.pathname;
  const trimmedPath = path.replace(/^\/+|\/+$/g, "");

  const queryStringObject = parsedURL.query;

  const decoder = new StringDecoder("utf-8");
  let buffer = "";
  request.on("data", (chunk) => {
    buffer += decoder.write(chunk);
  });
  request.on("end", () => {
    buffer += decoder.end();

    log({
      method,
      headers,
      path: trimmedPath,
      queryString: queryStringObject,
      payload: buffer,
    });

    const routerExists = Boolean(router[trimmedPath]);

    const chosenHandler = routerExists
      ? router[trimmedPath]
      : handlers.notFound;

    const data = {
      trimmedPath,
      queryStringObject,
      method,
      headers,
      payload: buffer,
    };

    chosenHandler(data, (statusCode, payload) => {
      statusCode = typeof statusCode === "number" ? statusCode : 200;
      payload = typeof payload === "object" ? payload : {};

      const payloadString = JSON.stringify(payload);

      response.setHeader("Content-Type", "application/json");
      response.writeHead(statusCode);
      response.end(payloadString);
    });
  });
}

const httpServer = http.createServer((request, response) => {
  handleRequest(request, response);
});

const handlers = {};
handlers.home = (data, callback) => {
  callback(200, { message: "Ok" });
};
handlers.notFound = (data, callback) => {
  callback(404);
};

const router = {
  home: handlers.home,
};

httpServer.listen(config.port.http, () =>
  console.log(
    `Server listening on port ${
      config.port.http
    } [${config.envName.toUpperCase()}]`
  )
);

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const httpServerOptions = {
  key: fs.readFileSync(path.resolve(__dirname, "..", "https", "key.pem")),
  cert: fs.readFileSync(path.resolve(__dirname, "..", "https", "cert.pem")),
};

const httpsServer = https.createServer(
  httpServerOptions,
  (request, response) => {
    handleRequest(request, response);
  }
);

httpsServer.listen(config.port.https, () =>
  console.log(
    `Server listening on port ${
      config.port.https
    } [${config.envName.toUpperCase()}]`
  )
);
