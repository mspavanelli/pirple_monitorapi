const environments = {};

environments.staging = {
  port: {
    http: 3333,
    https: 3334,
  },
  envName: "staging",
};

environments.production = {
  port: {
    http: 4200,
    https: 4201,
  },
  envName: "production",
};

const currentEnvironment =
  typeof process.env.NODE_ENV === "string" &&
  process.env.NODE_ENV.toLocaleLowerCase();

const environmentsToExport =
  typeof environments[currentEnvironment] === "object"
    ? environments[currentEnvironment]
    : environments.staging;

export default environmentsToExport;
